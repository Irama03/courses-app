// file with mocked data
const LOGOUT_BUTTON_TEXT = 'Logout';

const AUTHORS = 'Authors: ';
const DURATION = 'Duration: ';
const CREATED = 'Created: ';

const SHOW_COURSE_BUTTON_TEXT = 'Show course';

const SEARCH_PLACEHOLDER = 'Enter course name or id...';
const SEARCH_BUTTON_TEXT = 'Search';

const ADD_COURSE_BUTTON_TEXT = 'Add new course';
const CREATE_COURSE_BUTTON_TEXT = 'Create course';

const TITLE = 'Title';
const TITLE_PLACEHOLDER = 'Enter title...';

const DESCRIPTION = 'Description';
const DESCRIPTION_PLACEHOLDER = 'Enter description';

const ADD_AUTHOR = 'Add author';
const AUTHOR_NAME = 'Author name';
const AUTHOR_PLACEHOLDER = 'Enter author name...';
const CREATE_AUTHOR_BUTTON_TEXT = 'Create author';

const ADD_DURATION = 'Duration';
const DURATION_PLACEHOLDER = 'Enter duration in minutes...';

const ADD_AUTHORS = 'Authors';
const COURSE_AUTHORS = 'Course authors';
const DELETE_AUTHOR = 'Delete author';
const NO_AUTHORS = 'Author list is empty';

const ALERT = 'Please, fill in all fields';

const mockedCoursesList = [
	{
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		],
	},
	{
		id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
		title: 'Angular',
		description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
		creationDate: '10/11/2020',
		duration: 210,
		authors: [
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		],
	},
];

const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Nicolas Kim',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Anna Sidorenko',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Valentina Larina',
	},
];

export {
	LOGOUT_BUTTON_TEXT,
	AUTHORS,
	DURATION,
	CREATED,
	SHOW_COURSE_BUTTON_TEXT,
	mockedCoursesList,
	mockedAuthorsList,
	SEARCH_PLACEHOLDER,
	SEARCH_BUTTON_TEXT,
	ADD_COURSE_BUTTON_TEXT,
	CREATE_COURSE_BUTTON_TEXT,
	TITLE,
	TITLE_PLACEHOLDER,
	DESCRIPTION,
	DESCRIPTION_PLACEHOLDER,
	ADD_AUTHOR,
	AUTHOR_NAME,
	AUTHOR_PLACEHOLDER,
	CREATE_AUTHOR_BUTTON_TEXT,
	ADD_DURATION,
	DURATION_PLACEHOLDER,
	ADD_AUTHORS,
	COURSE_AUTHORS,
	DELETE_AUTHOR,
	NO_AUTHORS,
	ALERT,
};
