// a helper to get current date in the correct format
function getCurrentDate() {
	const d = new Date();
	return d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
}

export { getCurrentDate };
