function getCopyOfArray(arr) {
	const res = [];
	for (const x of arr) {
		res.push(x);
	}
	return res;
}

function isEmpty(str) {
	return str.trim() === '';
}

function isShorterThen2Chars(str) {
	return str.trim().length < 2;
}

export { getCopyOfArray, isEmpty, isShorterThen2Chars };
