function formatDate(date) {
	const d = date.split('/');
	if (d[0].length === 1) {
		d[0] = '0' + d[0];
	}
	if (d[1].length === 1) {
		d[1] = '0' + d[1];
	}
	return d.join('.');
}

export { formatDate };
