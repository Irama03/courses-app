// a helper to format course duration
function getDuration(duration) {
	if (parseInt(duration) < 0) return '00:00 hours';
	let hours = parseInt(duration / 60);
	if (hours < 10) {
		hours = '0' + hours;
	}
	let minutes = duration % 60;
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	return hours + ':' + minutes + ' hours';
}

export { getDuration };
