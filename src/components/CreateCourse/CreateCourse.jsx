import { Button } from '../../common/Button/Button';
import {
	CREATE_COURSE_BUTTON_TEXT,
	TITLE,
	TITLE_PLACEHOLDER,
	DESCRIPTION,
	DESCRIPTION_PLACEHOLDER,
	ADD_AUTHOR,
	AUTHOR_NAME,
	AUTHOR_PLACEHOLDER,
	CREATE_AUTHOR_BUTTON_TEXT,
	ADD_DURATION,
	DURATION_PLACEHOLDER,
	ADD_AUTHORS,
	COURSE_AUTHORS,
	DELETE_AUTHOR,
	mockedCoursesList,
	mockedAuthorsList,
	DURATION,
	NO_AUTHORS,
	ALERT,
} from '../../constants.js';
import './CreateCourse.css';
import { Input } from '../../common/Input/Input';
import { useState, useCallback, useEffect } from 'react';
import { getDuration } from '../../helpers/pipeDuration';
import {
	getCopyOfArray,
	isEmpty,
	isShorterThen2Chars,
} from '../../helpers/utils';
import { v4 as uuidv4 } from 'uuid';
import { getCurrentDate } from '../../helpers/dateGeneratop';

function CreateCourse({
	setShowCourses,
	initialCourses,
	/* setInitialCourses, */
	initialAuthors,
	setInitialAuthors,
}) {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [authorName, setAuthorName] = useState('');
	const [duration, setDuration] = useState('');
	const [authors, setAuthors] = useState([]);
	const [courseAuthors, setCourseAuthors] = useState([]);

	const getCopyOfInitialAuthors = useCallback(() => {
		return getCopyOfArray(initialAuthors);
	}, [initialAuthors]);

	useEffect(() => {
		setAuthors(getCopyOfInitialAuthors());
	}, [getCopyOfInitialAuthors]);

	const createCourse = () => {
		if (
			isEmpty(title) ||
			isShorterThen2Chars(description) ||
			duration < 1 ||
			courseAuthors.length === 0
		) {
			alert(ALERT);
		} else {
			const course = {
				id: uuidv4(),
				title: title,
				description: description,
				creationDate: getCurrentDate(),
				duration: duration,
				authors: courseAuthors.map((author) => author.id),
			};
			const c = getCopyOfArray(initialCourses);
			c.push(course);
			mockedCoursesList.push(course);
			// setInitialCourses(c);
			setShowCourses(true);
		}
	};

	const createAuthor = () => {
		if (isShorterThen2Chars(authorName)) {
			alert(ALERT);
		} else {
			const a = getCopyOfArray(initialAuthors);
			const a1 = getCopyOfArray(authors);
			const author = {
				id: uuidv4(),
				name: authorName,
			};
			a.push(author);
			a1.push(author);
			mockedAuthorsList.push(author);
			setInitialAuthors(a);
			setAuthors(a);
			setAuthorName('');
		}
	};

	const addAuthor = (author) => {
		const authors1 = authors.filter((a) => a.id !== author.id);
		const courseAuthors1 = getCopyOfArray(courseAuthors);
		courseAuthors1.push(author);
		setAuthors(authors1);
		setCourseAuthors(courseAuthors1);
	};

	const deleteAuthor = (author) => {
		const authors1 = getCopyOfArray(authors);
		authors1.push(author);
		const courseAuthors1 = courseAuthors.filter((a) => a.id !== author.id);
		setAuthors(authors1);
		setCourseAuthors(courseAuthors1);
	};

	const checkDuration = (dur) => {
		let d = dur;
		while (d.charCodeAt(0) < 48 || d.charCodeAt(0) > 57) {
			d = d.slice(1);
		}
		setDuration(d);
	};

	return (
		<div className='createCourse'>
			<div className='rowWithTitle'>
				<Button
					text={CREATE_COURSE_BUTTON_TEXT}
					onClick={createCourse}
					id='createCourse'
				/>
				<Input
					labelText={TITLE}
					placeholder={TITLE_PLACEHOLDER}
					onChange={setTitle}
					id='title'
					value={title}
				/>
			</div>
			<div className='descriptionDiv'>
				<p>{DESCRIPTION}</p>
				<textarea
					rows='6'
					cols='53'
					id='description'
					placeholder={DESCRIPTION_PLACEHOLDER}
					value={description}
					onChange={(event) => {
						setDescription(event.target.value);
					}}
				/>
			</div>
			<div className='authorsDuration'>
				<div className='oneRow'>
					<div className='leftDiv'>
						<h4 className='aTitle'>{ADD_AUTHOR}</h4>
						<Input
							labelText={AUTHOR_NAME}
							placeholder={AUTHOR_PLACEHOLDER}
							onChange={setAuthorName}
							id='authorName'
							value={authorName}
						/>
						<Button
							text={CREATE_AUTHOR_BUTTON_TEXT}
							onClick={createAuthor}
							id='createAuthor'
						/>
					</div>
					<div className='authors'>
						<h4 className='aTitle'>{ADD_AUTHORS}</h4>
						{authors.length === 0 ? (
							NO_AUTHORS
						) : (
							<ul className='ulMarginLeft'>
								{authors.map((author) => (
									<div key={author.id} className='authorInList'>
										<p className='mRight'>{author.name}</p>
										<Button
											text={ADD_AUTHOR}
											onClick={() => addAuthor(author)}
											id='addAuthor'
										/>
									</div>
								))}
							</ul>
						)}
					</div>
				</div>
				<div className='oneRow'>
					<div className='leftDiv'>
						<h4 className='aTitle'>{ADD_DURATION}</h4>
						<Input
							labelText={ADD_DURATION}
							placeholder={DURATION_PLACEHOLDER}
							onChange={checkDuration}
							id='duration'
							value={duration}
						/>
						<p className='durationLine'>
							{DURATION} {getDuration(duration)}
						</p>
					</div>
					<div className='authors'>
						<h4 className='aTitle'>{COURSE_AUTHORS}</h4>
						{courseAuthors.length === 0 ? (
							NO_AUTHORS
						) : (
							<ul className='ulMarginLeft'>
								{courseAuthors.map((author) => (
									<div key={author.id} className='authorInList'>
										<p className='mRight'>{author.name}</p>
										<Button
											text={DELETE_AUTHOR}
											onClick={() => deleteAuthor(author)}
											id='deleteAuthor'
										/>
									</div>
								))}
							</ul>
						)}
					</div>
				</div>
			</div>
		</div>
	);
}

export default CreateCourse;
