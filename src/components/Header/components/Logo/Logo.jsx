import React from 'react';
import logo from '../../../../assets/logo.png';
import './Logo.css';

export const Logo = () => <img src={logo} alt='Logo' className='logo' />;
