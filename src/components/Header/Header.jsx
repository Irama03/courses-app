import { Button } from '../../common/Button/Button';
import { Logo } from './components/Logo/Logo';
import { LOGOUT_BUTTON_TEXT } from '../../constants.js';
import './Header.css';

function Header(props) {
	const logout = () => {};

	return (
		<div className='header'>
			<Logo />
			<p className='justify-end name'>Ira</p>
			<Button text={LOGOUT_BUTTON_TEXT} onClick={logout} id='logout' />
		</div>
	);
}

export default Header;
