import {
	AUTHORS,
	DURATION,
	CREATED,
	SHOW_COURSE_BUTTON_TEXT,
} from '../../../../constants';
import { getDuration } from '../../../../helpers/pipeDuration';
import { formatDate } from '../../../../helpers/pipeDate';
import { Button } from '../../../../common/Button/Button';
import './CourseCard.css';

function CourseCard({ course, authors }) {
	const showCourse = () => {};

	return (
		<div className='courseCard'>
			<h2 className='firstColumn firstRow'>{course.title}</h2>
			<p className='firstColumn secondToFourthRow'>{course.description}</p>
			<p className='secondColumn firstRow hideText'>
				<strong>{AUTHORS}</strong>
				{authors}
			</p>
			<p className='secondColumn secondRow'>
				<strong>{DURATION}</strong>
				{getDuration(course.duration)}
			</p>
			<p className='secondColumn thirdRow'>
				<strong>{CREATED}</strong>
				{formatDate(course.creationDate)}
			</p>
			<Button
				text={SHOW_COURSE_BUTTON_TEXT}
				id='showCourse'
				onClick={showCourse}
			/>
		</div>
	);
}

export default CourseCard;
