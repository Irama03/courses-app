import { Button } from '../../../../common/Button/Button';
import {
	SEARCH_PLACEHOLDER,
	SEARCH_BUTTON_TEXT,
} from '../../../../constants.js';
import './SearchBar.css';
import { Input } from '../../../../common/Input/Input';

function SearchBar({ search, searchText, setSearchText }) {
	return (
		<div className='searchBar'>
			<Input
				labelText=''
				placeholder={SEARCH_PLACEHOLDER}
				onChange={setSearchText}
				id='searchText'
				value={searchText}
			/>
			<Button text={SEARCH_BUTTON_TEXT} onClick={search} id='searchButton' />
		</div>
	);
}

export default SearchBar;
