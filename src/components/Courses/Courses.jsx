import CourseCard from './components/CourseCard/CourseCard';
import {
	mockedCoursesList,
	mockedAuthorsList,
	ADD_COURSE_BUTTON_TEXT,
} from '../../constants';
import './Courses.css';
import SearchBar from './components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';
import { useCallback, useEffect, useState } from 'react';
import { getCopyOfArray } from '../../helpers/utils';

function Courses({
	setShowCourses,
	initialCourses,
	setInitialCourses,
	initialAuthors,
	setInitialAuthors,
}) {
	const [courses, setCourses] = useState([]);
	const [searchText, setSearchText] = useState('');

	const getCopyOfInitialCourses = useCallback(() => {
		return getCopyOfArray(initialCourses);
	}, [initialCourses]);

	useEffect(() => {
		setInitialCourses(mockedCoursesList);
		setCourses(getCopyOfInitialCourses());
		setInitialAuthors(mockedAuthorsList);
	}, [setInitialCourses, getCopyOfInitialCourses, setInitialAuthors]);

	const search = () => {
		let sT = searchText.trim();
		let c = [];
		if (sT === '') {
			c = getCopyOfInitialCourses();
			setSearchText('');
		} else {
			sT = sT.toLowerCase();
			const sTUpper = sT.toUpperCase();
			for (const course of initialCourses) {
				for (const i in sT) {
					if (
						course.title.includes(sT[i]) ||
						course.title.includes(sTUpper[i]) ||
						course.id.includes(sT[i]) ||
						course.id.includes(sTUpper[i])
					) {
						c.push(course);
						break;
					}
				}
			}
		}
		setCourses(c);
	};

	const getAuthorsNames = (authorsIds) => {
		const names = [];
		for (const authorId of authorsIds) {
			names.push(initialAuthors.find((a) => a.id === authorId).name);
		}
		return names.join(', ');
	};

	const addCourse = () => {
		setShowCourses(false);
	};

	return (
		<div className='courses'>
			<div className='rowWithSearch'>
				<SearchBar
					search={search}
					searchText={searchText}
					setSearchText={setSearchText}
				/>
				<Button
					text={ADD_COURSE_BUTTON_TEXT}
					onClick={addCourse}
					id='addCourse'
				/>
			</div>
			<ul className='ulMarginLeft'>
				{courses.map((course) => (
					<CourseCard
						key={course.id}
						course={course}
						authors={getAuthorsNames(course.authors)}
					/>
				))}
			</ul>
		</div>
	);
}

export default Courses;
