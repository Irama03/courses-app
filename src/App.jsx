import './App.css';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { useState } from 'react';
import CreateCourse from './components/CreateCourse/CreateCourse';

function App() {
	const [showCourses, setShowCourses] = useState(true);
	const [initialCourses, setInitialCourses] = useState([]);
	const [initialAuthors, setInitialAuthors] = useState([]);

	return (
		<>
			<Header />
			{showCourses ? (
				<Courses
					setShowCourses={setShowCourses}
					initialCourses={initialCourses}
					setInitialCourses={setInitialCourses}
					initialAuthors={initialAuthors}
					setInitialAuthors={setInitialAuthors}
				/>
			) : (
				<CreateCourse
					setShowCourses={setShowCourses}
					initialCourses={initialCourses}
					/* setInitialCourses={setInitialCourses} */
					initialAuthors={initialAuthors}
					setInitialAuthors={setInitialAuthors}
				/>
			)}
		</>
	);
}
export default App;
