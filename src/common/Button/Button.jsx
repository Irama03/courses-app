import './Button.css';

export const Button = ({ text, onClick, id }) => (
	<button onClick={onClick} className='buttons' id={id}>
		{text}
	</button>
);
