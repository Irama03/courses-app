import './Input.css';

export const Input = ({ labelText, placeholder, onChange, id, value }) => (
	<label htmlFor={id}>
		{labelText}
		<input
			type='text'
			placeholder={placeholder}
			onChange={(event) => {
				onChange(event.target.value);
			}}
			id={id}
			value={value}
		/>
	</label>
);
